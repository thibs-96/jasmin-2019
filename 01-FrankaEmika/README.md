# INSTALLATION DU FRANKA PANDA

**Remarque**: Il est déconseillé d’installer libfranka depuis les dépôts ROS, car les paquets peuvent ne pas être à jour et peuvent causer des conflits avec d’autres bibliothèques comme ViSP du fait de leur instabilité et de leur mise à jour quasi-quotidienne. Il est donc conseillé de suivre les consignes d’installation suivantes :

```
$ sudo apt install build-essential cmake git libpoco-dev libeigen3-dev
$ cd ~/visp_ws
$ git clone --recursive https://github.com/frankaemika/libfranka
$ cd libfranka$ mkdir build$ cd build$ cmake .. -DCMAKE_BUILD_TYPE=Release
$ make -j4$ sudo make install
```

**Remarque** : Pour l’asservissement visuel, il sera aussi nécessaire de configurer le noyau Linux en temps-réel.