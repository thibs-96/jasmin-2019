# CMake generated Testfile for 
# Source directory: /home/loic/visp-ws/libfranka
# Build directory: /home/loic/visp-ws/libfranka
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(common)
subdirs(test)
subdirs(examples)
