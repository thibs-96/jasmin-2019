#include <iostream>

//#include <visp3/core/vpImageConvert.h>
//#include <visp3/core/vpCameraParameters.h>
//#include <visp3/core/vpXmlParserCamera.h>
#include <visp3/gui/vpDisplayGDI.h>
#include <visp3/gui/vpDisplayX.h>
#include <visp3/io/vpImageIo.h>
//#include <visp3/sensor/vpRealSense2.h>
#include <visp3/robot/vpRobotFranka.h>
#include <visp3/detection/vpDetectorAprilTag.h>
#include <visp3/visual_features/vpFeatureThetaU.h>
#include <visp3/visual_features/vpFeatureTranslation.h>
#include <visp3/vs/vpServo.h>
#include <visp3/gui/vpPlot.h>

#if defined(VISP_HAVE_REALSENSE2) && defined(VISP_HAVE_CPP11_COMPATIBILITY) &&                                         \
  (defined(VISP_HAVE_X11) || defined(VISP_HAVE_GDI)) && defined(VISP_HAVE_FRANKA)

//capture des paramètres
int main(int argc, char **argv)
{
  
  std::string opt_robot_ip = "192.168.1.1";
  std::string opt_eMc_filename = "";
  bool display_tag = true;
  int opt_quad_decimate = 2;
  bool opt_verbose = false;
  bool opt_plot = false;
  bool opt_adaptive_gain = false;
  bool opt_task_sequencing = false;
  double convergence_threshold_t = 0.0005, convergence_threshold_tu = vpMath::rad(0.5);

  for (int i = 1; i < argc; i++) {
    
    if (std::string(argv[i]) == "--ip" && i + 1 < argc) {
      opt_robot_ip = std::string(argv[i + 1]);
    }
    else if (std::string(argv[i]) == "--eMc" && i + 1 < argc) {
      opt_eMc_filename = std::string(argv[i + 1]);
    }
    
    else if (std::string(argv[i]) == "--help" || std::string(argv[i]) == "-h") {
      std::cout << argv[0] << " [--ip <default " << opt_robot_ip << ">] [--tag_size <marker size in meter; default " << opt_tagSize << ">] [--eMc <eMc extrinsic file>] "
                           << "[--quad_decimate <decimation; default " << opt_quad_decimate << ">] [--adaptive_gain] [--plot] [--task_sequencing] [--verbose] [--help] [-h]"
                           << "\n";
      return EXIT_SUCCESS;
    }
  }
  vpRobotFranka robot;

  // connection avec le robot
  try {
  robot.connect(opt_robot_ip);

  // Asservissement visuel 
  // Servo
  vpHomogeneousMatrix cdMc, cMo, cdMo, oMo;

  //Coordionnées des 4 points
  
  vpPoint point[4];
  point[0].setWorldCoordinates(-0.045, -0.045, 0);
  point[3].setWorldCoordinates(-0.045, 0.045, 0);
  point[2].setWorldCoordinates(0.045, 0.045, 0);
  point[1].setWorldCoordinates(0.045, -0.045, 0);









  // Desired pose to reach
  cdMo[0][0] = 1; cdMo[0][1] =  0; cdMo[0][2] =  0;
  cdMo[1][0] = 0; cdMo[1][1] = -1; cdMo[1][2] =  0;
  cdMo[2][0] = 0; cdMo[2][1] =  0; cdMo[2][2] = -1;
  cdMo[0][3] = 0;
  cdMo[1][3] = 0;
  cdMo[2][3] = 0.3; // 30 cm along camera z axis
  cdMc = cdMo * cMo.inverse();




  // computes the point coordinates in the camera frame and its 2D
  // coordinates
for (unsigned int i = 0; i < 4; i++) {
      point[i].track(cdMo);
      vpFeatureBuilder::create(pd[i], point[i]);
      point[i].track(cMo);
      vpFeatureBuilder::create(p[i], point[i]);
      task.addFeature(p[i], pd[i]);
    }



  vpServo task;
  task.setServo(vpServo::EYEINHAND_CAMERA);
  task.setInteractionMatrixType(vpServo::CURRENT);






  if (opt_adaptive_gain) {
      vpAdaptiveGain lambda(1.5, 0.4, 30); // lambda(0)=4, lambda(oo)=0.4 and lambda'(0)=30
      task.setLambda(lambda);
    }
    else {
      task.setLambda(0.5);
    }

    vpHomogeneousMatrix wMc, wMo;
    cMo = robot.get_cMo();
    robot.getPosition(wMc);
    wMo = wMc * cMo;


    for (unsigned int iter = 0; iter < 150; iter++) {
        robot.getPosition(wMc);
        // Get the current pose of the camera
        cMo = robot.get_cMo();
        cMo = wMc.inverse() * wMo;
        for (unsigned int i = 0; i < 4; i++) {
            point[i].track(cMo);
            vpFeatureBuilder::create(p[i], point[i]);
        }
        vpColVector v = task.computeControlLaw();
        robot.setVelocity(vpRobot::CAMERA_FRAME, v);

    
    }

    task.kill();
  }
