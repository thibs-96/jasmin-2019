file(REMOVE_RECURSE
  "CMakeFiles/run_all_tests.dir/calculations_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/control_loop_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/control_types_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/duration_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/errors_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/gripper_command_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/gripper_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/helpers.cpp.o"
  "CMakeFiles/run_all_tests.dir/logger_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/lowpass_filter_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/mock_server.cpp.o"
  "CMakeFiles/run_all_tests.dir/model_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/rate_limiting_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/robot_command_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/robot_impl_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/robot_state_tests.cpp.o"
  "CMakeFiles/run_all_tests.dir/robot_tests.cpp.o"
  "run_all_tests.pdb"
  "run_all_tests"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/run_all_tests.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
