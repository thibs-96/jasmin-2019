# CMake generated Testfile for 
# Source directory: /home/loic/visp-ws/libfranka/test
# Build directory: /home/loic/visp-ws/libfranka/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(Default "run_all_tests" "--gtest_output=xml:/home/loic/visp-ws/libfranka/test_results/default.xml")
add_test(ASan "/home/loic/visp-ws/libfranka/test/run_all_tests_asan" "--gtest_output=xml:/home/loic/visp-ws/libfranka/test_results/asan.xml")
add_test(UBSan "/home/loic/visp-ws/libfranka/test/run_all_tests_ubsan" "--gtest_output=xml:/home/loic/visp-ws/libfranka/test_results/ubsan.xml")
add_test(TSan "/home/loic/visp-ws/libfranka/test/run_all_tests_tsan" "--gtest_output=xml:/home/loic/visp-ws/libfranka/test_results/tsan.xml")
subdirs(../googletest)
