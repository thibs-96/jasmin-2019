# VISUAL SERVOING

**CONTENU DU PAQUET**

`librealsense` : Bibliothèque pour la Intel RealSense D435

`visp` : paquet ViSP

`visp-build` : build de la source ViSP

## Installation des bibliothèques
### Installation des dépendances
`$ sudo apt-get install build-essential bc curl ca-certificates fakeroot gnupg2 libssl-dev lsb-release`

### Téléchargement de la version du noyau
`$ curl -SLO https://www.kernel.org/pub/linux/kernel/v4.x/linux-4.14.12.tar.xz`

`$ curl -SLO https://www.kernel.org/pub/linux/kernel/projects/rt/4.14/older/patch-4.14.12-rt10.patch.xz`

### Décompression
`$ xz -d linux-4.14.12.tar.xz$ xz -d patch-4.14.12-rt10.patch.xz`

### Compilation du noyau
`$ tar xf linux-4.14.12.tar$ cd linux-4.14.12$ patch -p1 < ../patch-4.14.12-rt10.patch`

### Configuration du noyau
`$ make oldconfig`

Cela ouvre un menu de choix. Choisir `Fully Preemptible Kernel` :

```
Preemption Model
1. No Forced Preemption (Server) (PREEMPT_NONE)
2. Voluntary Kernel Preemption (Desktop) (PREEMPT_VOLUNTARY)
3. Preemptible Kernel (Low-Latency Desktop) (PREEMPT__LL) (NEW)
4. Preemptible Kernel (Basic RT) (PREEMPT_RTB) (NEW)
> 5. Fully Preemptible Kernel (RT) (PREEMPT_RT_FULL) (NEW)
```

### Choix du nombre de cœurs du CPU
`$ fakeroot make -j4 deb-pkg`

### Installation
`$ sudo dpkg -i ../linux-headers-4.14.12-rt10_*.deb ../linux-image-4.14.12-rt10_*.deb`

### Redémarrage
`$ sudo reboot`

### Vérification de la version du noyau
`$ uname -msrLinux 4.14.12 x86_64`

### Ajout de l’utilisateur au groupe realtime
`$ sudo addgroup realtime$ sudo usermod -a -G realtime $(whoami)`

Ajout des limites suivantes dans `/etc/security/limits.conf`
```
@realtime soft rtprio 99
@realtime soft priority 99
@realtime soft memlock 102400
@realtime hard rtprio 99
@realtime hard priority 99
@realtime hard memlock 102400
```

**Remarque** : En cas de problème de détection du noyau temps réel lors de l'exécution des programmes d'exemple, commenter les lignes suivantes dans `visp_ws/libfranka/src/control_loop.cpp` :

```
If (throw_on_error && !hasRealtimekernel()) {
	Throw RealtimeExecption (“libfranka: Running kernel does not have realtime capabilities. “);
}

```

## Installation de la caméra Intel RealSense D435

### Téléchargement des paquets

`$ cd ~/visp_ws$ git clone https://github.com/IntelRealSense/librealsense.git$ cd librealsense`

### Débrancher la caméra
`$ sudo apt-get install libssl-dev`

### Installation de udev rules dans le dossier librealsense
`$ sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/`
`$ sudo udevadm control --reload-rules && udevadm trigger`

### Installation des paquet pour le build de la bibliothèque librealsense:
`$ sudo apt-get install libusb-1.0-0-dev pkg-config libgtk-3-dev`
`$ sudo apt-get install libglfw3-dev`

### Installation et construction des paquets de la bibliothèque librealsense
`$ mkdir build$ cd build$ cmake .. -DBUILD_EXAMPLES=ON -DCMAKE_BUILD_TYPE=Release$ make -j4`
`$ sudo make install`

### Test de la caméra
`$ ./examples/capture/rs-capture`

## ViSP

### Installation
 ```
 $ cd $VISP_WS
$ git clone https://github.com/lagadic/visp.git
$ mkdir visp-build
$ cd visp-build
$ cmake ../visp -DCMAKE_INSTALL_PREFIX=/usr
$ make -j4
$ sudo make install
```

**Remarque** : ne pas oublier de changer le chemin d’accès lors de l’installation de ViSP de `usr/local/` à `usr/`

### Calibration extrinsèque de la caméra
```
$ cd visp_ws
$ cd visp
$ svn export https://github.com/lagadic/visp.git/trunk/tutorial/calibration
$ ./tutorial-chessboard-pose --square_size 0.027 --input image-%d.png
```
