# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/lz4/lz4.c" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/lz4/lz4.c.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/roslz4/src/lz4s.c" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/roslz4/src/lz4s.c.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/roslz4/src/xxhash.c" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/roslz4/src/xxhash.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_C
  "BUILD_EASYLOGGINGPP"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "UNICODE"
  "USE_SYSTEM_LIBUSB"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "third-party/realsense-file/rosbag/console_bridge/include"
  "third-party/realsense-file/rosbag/cpp_common/include"
  "third-party/realsense-file/rosbag/rosbag_storage/include"
  "third-party/realsense-file/rosbag/roscpp_serialization/include"
  "third-party/realsense-file/rosbag/rostime/include"
  "third-party/realsense-file/rosbag/roscpp_traits/include"
  "third-party/realsense-file/rosbag/roslz4/include"
  "third-party/realsense-file/rosbag/msgs"
  "third-party/realsense-file/boost"
  "third-party/realsense-file/lz4/lib"
  )
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/console_bridge/src/console.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/console_bridge/src/console.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/cpp_common/src/debug.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/cpp_common/src/debug.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/cpp_common/src/header.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/cpp_common/src/header.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/bag.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/bag.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/bag_player.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/bag_player.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/buffer.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/buffer.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/chunked_file.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/chunked_file.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/lz4_stream.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/lz4_stream.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/message_instance.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/message_instance.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/query.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/query.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/stream.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/stream.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/uncompressed_stream.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/uncompressed_stream.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rosbag_storage/src/view.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rosbag_storage/src/view.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/roscpp_serialization/src/serialization.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/roscpp_serialization/src/serialization.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rostime/src/duration.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rostime/src/duration.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rostime/src/rate.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rostime/src/rate.cpp.o"
  "/home/loic/visp-ws/librealsense/third-party/realsense-file/rosbag/rostime/src/time.cpp" "/home/loic/visp-ws/librealsense/third-party/realsense-file/CMakeFiles/realsense-file.dir/rosbag/rostime/src/time.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "BUILD_EASYLOGGINGPP"
  "ELPP_NO_DEFAULT_LOG_FILE"
  "ELPP_THREAD_SAFE"
  "HWM_OVER_XU"
  "RS2_USE_V4L2_BACKEND"
  "UNICODE"
  "USE_SYSTEM_LIBUSB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "third-party/realsense-file/rosbag/console_bridge/include"
  "third-party/realsense-file/rosbag/cpp_common/include"
  "third-party/realsense-file/rosbag/rosbag_storage/include"
  "third-party/realsense-file/rosbag/roscpp_serialization/include"
  "third-party/realsense-file/rosbag/rostime/include"
  "third-party/realsense-file/rosbag/roscpp_traits/include"
  "third-party/realsense-file/rosbag/roslz4/include"
  "third-party/realsense-file/rosbag/msgs"
  "third-party/realsense-file/boost"
  "third-party/realsense-file/lz4/lib"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
