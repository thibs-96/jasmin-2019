# Install script for directory: /home/loic/visp-ws/visp/example

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/loic/visp-ws/visp/example/calibration/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/coin-simulator/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/device/display/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/device/framegrabber/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/device/kinect/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/device/laserscanner/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/device/light/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/direct-visual-servoing/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/homography/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/image/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/key-point/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/manual/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/math/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/moments/image/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/moments/points/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/moments/polygon/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/ogre-simulator/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/parse-argv/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/pose-estimation/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/robot-simulator/afma6/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/robot-simulator/camera/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/robot-simulator/viper850/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-afma4/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-afma6/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-biclops/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-franka/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-pioneer/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-ptu46/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-viper650/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/servo-viper850/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/tools/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/tracking/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/video/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/example/wireframe-simulator/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/loic/visp-ws/visp/example/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
