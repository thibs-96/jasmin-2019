# Install script for directory: /home/loic/visp-ws/visp/tutorial

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/loic/visp-ws/visp/tutorial/bridge/opencv/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/calibration/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/computer-vision/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/grabber/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/detection/barcode/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/detection/dnn/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/detection/face/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/detection/matching/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/detection/object/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/detection/tag/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/image/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/imgproc/autothreshold/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/imgproc/brightness/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/imgproc/connected-components/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/imgproc/contour/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/imgproc/contrast-sharpening/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/imgproc/count-coins/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/imgproc/flood-fill/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/robot/pioneer/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/robot/mbot/raspberry/visp/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/simulator/image/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/trace/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/blob/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/keypoint/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/generic/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/generic-apriltag/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/generic-rgbd/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/generic-stereo/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/old/edges/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/old/generic/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/old/hybrid/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/old/keypoint/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/model-based/old/stereo-deprecated/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/template-tracker/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/tracking/moving-edges/cmake_install.cmake")
  include("/home/loic/visp-ws/visp/tutorial/visual-servo/ibvs/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/loic/visp-ws/visp/tutorial/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
