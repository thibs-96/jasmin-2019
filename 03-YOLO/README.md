# YOLO POUR LA DETECTION DE JASMIN
Ici sont détaillées les étapes pour la détection en temps réel des fleurs de jasmin à récolter, par l'algorithme de vision articielle [YOLO] (https://pjreddie.com/darknet/yolo/)

**CONTENU DU PAQUET** :

`ImageNet_Utils` : Utilitaire pour le téléchargement "massif" d'images sur ImageNet

`Yolo_mark` : Utilitaire pour le marquage des images pour entraînement par YOLO

`darknet` : Algorithme de détection temps-réel YOLO

## Darknet YOLO V3
Référence : https://github.com/AlexeyAB/darknet

**Prérequis**
- Linux GCC >= 4.9
- CUDA 8.0 / 9.0 (problèmes de compatibilité avec CUDA 10.0) avec pilote NVIDIA propriétaire
- OpenCV 3.3.0 ou 2.4.13

**Installation**
- Clonage du Git :
`git clone https://github.com/AlexeyAB/darknet.git`
- Aller dans le dossier cloné
`cd darknet`
- Editer le fichier `Makefile`
`nano Makefile`
- Activer le traitement par GPU (après installation des pilotes NVIDIA et de CUDA) et par OpenCV (après installation d'OpenCV) 
`GPU=1` et `OPENCV=1`
- Compiler le dossier 
`make`

**Préparation du set**
- Placer toutes les images dans le dossier `data/img`
- Utiliser l'utilitaire `Yolo_mark` pour définir les ROI des images du dataset
`git clone https://github.com/AlexeyAB/Yolo_mark.git` 
- Suivre les instructions disponibles sur la page

**Paramètres d'entraînement**
- Paramètres compatibles avec l'architecture matérielle utilisée (NVIDIA GTX 1050, CUDA 9.0, Intel Core i7 7700HQ, Ubuntu 16.04)
- En cas de soucis, se référer aux 2 pages GitHub
- `data/obj.data` : `classes = 2`
- `data/obj.names` : `jasmine-opened` et `jasmine-closed`
- `cfg/yolo-obj.cfg` : 
    - Décommenter les lignes `Training`;
    - `batch=64`
    - `subdivisions=32`
    - Dans chaque classe `[yolo]` :
        - `classes=2`
        - `random=0` (sinon plantage de CUDA)
- Lancer l'entraînement (avec calcul de la mAP et affichage du graphe)
    - `./darknet detector train data/obj.data cfg/yolo-obj.cfg`

**Tests et détection**
- Test sur une image : `./darknet detector test data/obj.data cfg/yolo-obj.cfg backup/yolo-obj_3000.weights data/img/ImageNet-Jasmine__3_.jpg `
- Test sur une vidéo : `./darknet detector demo data/obj.data cfg/yolo-obj.cfg backup/yolo-obj_3000.weights  data/IFF-Jasmine.mp4`

**Voies d'amélioration**
- Export vers un topic ROS
    - Les coordonnées de la bounding box peuvent déjà être récupérées dans le script `image.c`
        - l. 293-294 :
            ```
            // Print bounding box values 
            printf("Bounding Box: Left=%d, Top=%d, Right=%d, Bottom=%d\n", left, top, right, bot);
            ```