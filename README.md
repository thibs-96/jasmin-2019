# ROBOT FRANKA - PI 2019 (TH - LL)

Ce dossier contient l'ensemble des instructions d'installation du Robot Franka, de la mise en place de l'asservissement visuel par caméra Intel RealSense D435,
ainsi que la détection du jasmin.

## Installation du robot Franka Emika
Consignes d'installation et de prise en main du robot.

## Asservissement visuel
Consignes d'installation de la caméra et algorithme d'asservissement visuel.

## Algorithme de détection du jasmin
Consignes d'installation de YOLO + méthode d'entraînement de l'algorithme.